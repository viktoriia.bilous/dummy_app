Rails.application.routes.draw do
  get '/orders/:id', to: 'orders#show'
  post '/orders', to: 'orders#update'
end
