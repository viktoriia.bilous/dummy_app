require 'rails_helper'

describe OrdersController, type: :controller do
  let(:item) { FactoryBot.create(:item) }

  describe 'GET #show' do
    let(:order) { FactoryBot.create(:order) }

    context 'when order is not found by id' do
      it 'returns head with 404 status' do
        get :show, params: { id: 100 }

        expect(response).to have_http_status(:not_found)
      end
    end

    context 'when order is found by id' do
      it 'calls DiscountsService' do
        expect(DiscountsService).to receive(:apply_discounts)
          .with(order, current_user_time_zone: 0)

        get :show, params: { id: order.id }
      end

      it 'calls OrderPriceService' do
        expect(OrderPriceService).to receive(:update_price_and_discount).with(order)

        get :show, params: { id: order.id }
      end

      it 'returns order json' do
        get :show, params: { id: order.id }

        expect(JSON.parse(response.body)).to eq JSON(order.to_json)
      end
    end
  end

  describe 'POST #update' do
    let(:order_params) { { item_id: item.id, quantity: 1 } }

    context 'when id is not provided in params' do
      it 'creates new Order' do
        expect { post :update, params: { line: order_params } }
          .to change { Order.count }.by(1)
      end

      it 'calls OrderUpdateService' do
        expect(OrderUpdateService).to receive(:add_or_update_order_line)

        post :update, params: { line: order_params }
      end

      it 'returns order line json' do
        post :update, params: { line: order_params }

        expect(JSON.parse(response.body).with_indifferent_access)
          .to include order_params
      end
    end

    context 'when id is provided in params' do
      context 'when order is not found by id' do
        it 'returns head with 404 status' do
          post :update, params: { id: 100, line: order_params }

          expect(response).to have_http_status(:not_found)
        end
      end

      context 'when order is found by id' do
        let(:order) { FactoryBot.create(:order) }

        it 'calls OrderUpdateService' do
          expect(OrderUpdateService).to receive(:add_or_update_order_line)

          post :update, params: { id: order.id, line: order_params }
        end

        it 'returns order line json' do
          post :update, params: { id: order.id, line: order_params }

          expect(JSON.parse(response.body).with_indifferent_access)
            .to include order_params
        end
      end
    end
  end
end
