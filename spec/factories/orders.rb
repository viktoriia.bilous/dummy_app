FactoryBot.define do
  factory :payment_info
end

FactoryBot.define do
  factory :shipping_info
end

FactoryBot.define do
  factory :order do
    status 0
    total_price 0
    total_discount 0
  end
end

FactoryBot.define do
  factory :order_line do
    item
  end
end
