FactoryBot.define do
  factory :item do
    name Faker::Food.ingredient
    description Faker::Lorem.sentence
    color { Faker::Number.between(0, 6) }
    price Faker::Number.decimal(2)
    category_id { Faker::Number.between(1, 7) }

    factory :food_item do
      category_id Category::FOOD
    end

    factory :keyboard_item do
      category_id Category::KEYBOARD
    end

    factory :mouse_item do
      category_id Category::MOUSE
    end

    trait :white do
      color 1
    end

    trait :black do
      color 2
    end
  end
end
