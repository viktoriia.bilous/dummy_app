require 'rails_helper'

describe OrderUpdateService do
  let(:item) { FactoryBot.create(:item) }
  let(:line) { { item_id: item.id, quantity: 1 } }
  let(:order) { FactoryBot.create(:order) }
  let(:service_object) { described_class.new(order, line) }

  describe '.add_or_update_order_line' do
    before do
      allow(described_class).to receive(:new).and_return(service_object)
    end

    it 'creates service object' do
      expect(described_class).to receive(:new).with(order, line)

      described_class.add_or_update_order_line(order, line)
    end

    it 'calls "add_or_update_order_line" method' do
      expect(service_object).to receive(:add_or_update_order_line)

      described_class.add_or_update_order_line(order, line)
    end
  end

  describe '#add_or_update_order_line' do
    context 'when order does not have order line with provided item' do
      it 'creates new order line' do
        expect { service_object.add_or_update_order_line }
          .to change { order.order_lines.size }.by(1)
      end

      it 'sets order line quantity' do
        service_object.add_or_update_order_line

        expect(order.order_lines.find_by(item_id: line[:item_id]).quantity)
          .to eq line[:quantity]
      end
    end

    context 'when order already has order line with provided item' do
      let!(:order_line) { FactoryBot.create(:order_line, order: order, item: item) }
      let(:line) { { item_id: item.id, quantity: 2 } }

      it 'does not create new order line' do
        expect { service_object.add_or_update_order_line }
          .to_not(change { order.order_lines.size })
      end

      it 'sets order line quantity' do
        service_object.add_or_update_order_line

        expect(order.order_lines.find_by(item_id: line[:item_id]).quantity)
          .to eq line[:quantity]
      end
    end
  end
end
