require 'rails_helper'

describe DiscountsService do
  let(:order) { FactoryBot.create(:order) }
  let(:service_object) { described_class.new(order) }

  describe '.apply_discounts' do
    before do
      allow(described_class).to receive(:new).and_return(service_object)
    end

    it 'creates service object' do
      expect(described_class).to receive(:new).with(order, {})

      described_class.apply_discounts(order)
    end

    it 'calls "apply_discounts" method' do
      expect(service_object).to receive(:apply_discounts)

      described_class.apply_discounts(order)
    end
  end

  describe '#apply_discounts' do
    it 'calls each discount class' do
      DiscountsService::DISCOUNTS.each do |discount|
        expect(discount).to receive(:call)
      end

      service_object.apply_discounts
    end
  end
end
