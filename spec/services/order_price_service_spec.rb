require 'rails_helper'

describe OrderPriceService do
  let(:order) do
    order = FactoryBot.create(:order)
    FactoryBot.create(
      :order_line,
      item: FactoryBot.create(:food_item, price: 10),
      order: order,
      discount: 10,
      quantity: 10
    )

    FactoryBot.create(
      :order_line,
      item: FactoryBot.create(:food_item, price: 20),
      order: order,
      discount: 10,
      quantity: 10
    )

    order
  end

  let(:service_object) { described_class.new(order) }

  describe '.update_price_and_discount' do
    before do
      allow(described_class).to receive(:new).and_return(service_object)
    end

    it 'creates service object' do
      expect(described_class).to receive(:new).with(order)

      described_class.update_price_and_discount(order)
    end

    it 'calls "update_price_and_discount" method' do
      expect(service_object).to receive(:update_price_and_discount)

      described_class.update_price_and_discount(order)
    end
  end

  describe '#update_price_and_discount' do
    it 'calculates total price' do
      expect { service_object.update_price_and_discount }
        .to change { order.total_price }.from(0).to(300)
    end

    it 'calculates total discount' do
      expect { service_object.update_price_and_discount }
        .to change { order.total_discount }.from(0).to(30)
    end
  end
end
