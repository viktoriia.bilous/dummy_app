require 'rails_helper'

describe KeyboardMouseDiscount do
  let(:order) do
    order = FactoryBot.create(:order)
    FactoryBot.create(:order_line, item: FactoryBot.create(:food_item), order: order)
    FactoryBot.create(:order_line, item: FactoryBot.create(:keyboard_item), order: order)
    FactoryBot.create(:order_line, item: FactoryBot.create(:mouse_item), order: order)
    order
  end

  let(:discount) { described_class.new(order) }

  describe '#call' do
    context 'when order does not contain keyboard and mouse' do
      let(:order) do
        order = FactoryBot.create(:order)
        FactoryBot.create_list(:order_line, 2, item: FactoryBot.create(:food_item), order: order)
        order
      end

      it 'does not apply discounts' do
        discount.call

        order.order_lines.each do |line|
          expect(line.discount).to eq 0
        end
      end
    end

    context 'when order contains only keyboard or mouse' do
      let(:order) do
        order = FactoryBot.create(:order)
        FactoryBot.create(:order_line, item: FactoryBot.create(:food_item), order: order)
        FactoryBot.create(:order_line, item: FactoryBot.create(:keyboard_item), order: order)
        order
      end

      it 'does not apply discounts' do
        discount.call

        order.order_lines.each do |line|
          expect(line.discount).to eq 0
        end
      end
    end

    context 'when order contains keyboard and mouse' do
      it 'applies discount to order lines with keyboard or mouse items' do
        discount.call

        order.order_lines.each do |line|
          expect(line.discount).to eq 3 if line.item.category.keyboard? || line.item.category.mouse?
        end
      end

      it 'does not apply discount to order lines with non keyboard or mouse items' do
        discount.call

        order.order_lines.each do |line|
          unless line.item.category.keyboard? || line.item.category.mouse?
            expect(line.discount).to eq 0
          end
        end
      end
    end
  end
end
