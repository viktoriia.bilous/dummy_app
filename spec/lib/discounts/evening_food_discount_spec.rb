require 'rails_helper'

describe EveningFoodDiscount do
  let(:order) do
    order = FactoryBot.create(:order)
    FactoryBot.create_list(:order_line, 2, item: FactoryBot.create(:food_item), order: order)
    FactoryBot.create(:order_line, item: FactoryBot.create(:keyboard_item), order: order)
    order
  end

  let(:params) { { current_user_time_zone: 4 } }
  let(:discount) { described_class.new(order, params) }

  before do
    Time.zone = params[:current_user_time_zone]
  end

  describe '#call' do
    context 'when current user time is not available' do
      let(:params) { {} }

      it 'does not apply discounts' do
        discount.call

        order.order_lines.each do |line|
          expect(line.discount).to eq 0
        end
      end
    end

    context 'when current user time is before 6 PM' do
      before do
        Timecop.freeze(Time.zone.parse('1 pm'))
      end

      it 'does not apply discounts' do
        discount.call

        order.order_lines.each do |line|
          expect(line.discount).to eq 0
        end
      end
    end

    context 'when current user time is after 6 PM' do
      before do
        Timecop.freeze(Time.zone.parse('7 pm'))
      end

      it 'applies discount to order lines with food items' do
        discount.call

        order.order_lines.each do |line|
          expect(line.discount).to eq 5 if line.item.category.food?
        end
      end

      it 'does not apply discount to order lines with non food items' do
        discount.call

        order.order_lines.each do |line|
          expect(line.discount).to eq 0 unless line.item.category.food?
        end
      end
    end
  end
end
