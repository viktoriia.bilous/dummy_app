require 'rails_helper'

describe Discount do
  let(:order) { FactoryBot.create(:order) }
  let(:discount) { described_class.new(order) }

  describe '.call' do
    before do
      allow(described_class).to receive(:new).and_return(discount)
    end

    it 'creates discount object' do
      expect(described_class).to receive(:new).with(order, {})

      described_class.call(order)
    end

    it 'calls "call" method' do
      expect(discount).to receive(:call)

      described_class.call(order)
    end
  end

  describe '#call' do
    it 'checks if discount should be applied' do
      expect(discount).to receive(:apply?)

      discount.call
    end

    context 'when discount should be applied' do
      before do
        allow(discount).to receive(:apply?).and_return(true)
      end

      it 'applies discount' do
        expect(discount).to receive(:apply)

        discount.call
      end
    end

    context 'when discount should not be applied' do
      before do
        allow(discount).to receive(:apply?).and_return(false)
      end

      it 'does not apply discount' do
        expect(discount).to_not receive(:apply)

        discount.call
      end
    end
  end
end
