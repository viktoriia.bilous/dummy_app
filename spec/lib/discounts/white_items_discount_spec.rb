require 'rails_helper'

describe WhiteItemsDiscount do
  let(:order) do
    order = FactoryBot.create(:order)
    FactoryBot.create_list(
      :order_line,
      10,
      item: FactoryBot.create(:food_item, :white),
      order: order
    )
    FactoryBot.create(:order_line, item: FactoryBot.create(:food_item, :black), order: order)
    order
  end

  let(:discount) { described_class.new(order) }

  describe '#call' do
    context 'when order does not contain 10 white items' do
      let(:order) do
        order = FactoryBot.create(:order)
        FactoryBot.create_list(
          :order_line,
          9,
          item: FactoryBot.create(:food_item, :white),
          order: order
        )
        order
      end

      it 'does not apply discounts' do
        discount.call

        order.order_lines.each do |line|
          expect(line.discount).to eq 0
        end
      end
    end

    context 'when order contains 10 white items' do
      it 'applies discount to order lines with white items' do
        discount.call

        order.order_lines.each do |line|
          expect(line.discount).to eq 1 if line.item.white?
        end
      end

      it 'does not apply discount to order lines with non white items' do
        order.order_lines.each do |line|
          expect(line.discount).to eq 0 unless line.item.white?
        end
      end
    end
  end
end
