class WhiteItemsDiscount < Discount
  private

  def apply?
    order.order_lines.count { |order_line| order_line.item.white? } >= 10
  end

  def apply
    order.order_lines.each do |order_line|
      order_line.update(discount: order_line.discount + 1) if order_line.item.white?
    end
  end
end
