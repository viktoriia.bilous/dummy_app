class KeyboardMouseDiscount < Discount
  private

  def apply?
    order.order_lines.any? { |line| line.item.category.keyboard? } &&
      order.order_lines.any? { |line| line.item.category.mouse? }
  end

  def apply
    order.order_lines.each do |order_line|
      if order_line.item.category.keyboard? || order_line.item.category.mouse?
        order_line.update(discount: order_line.discount + 3)
      end
    end
  end
end
