class EveningFoodDiscount < Discount
  private

  def apply?
    return false unless params[:current_user_time_zone]

    Time.zone = params[:current_user_time_zone]
    Time.zone.now >= Time.zone.parse('6 pm')
  end

  def apply
    order.order_lines.each do |order_line|
      order_line.update(discount: order_line.discount + 5) if order_line.item.category.food?
    end
  end
end
