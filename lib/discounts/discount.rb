class Discount
  attr_reader :order, :params

  def self.call(order, params = {})
    new(order, params).call
  end

  def initialize(order, params = {})
    @order = order
    @params = params
  end

  def call
    apply if apply?
  end

  private

  def apply?; end

  def apply; end
end
