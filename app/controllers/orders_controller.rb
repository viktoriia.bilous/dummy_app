class OrdersController < ApplicationController
  before_action :find_or_create

  def show
    DiscountsService.apply_discounts(@order, discount_params)
    OrderPriceService.update_price_and_discount(@order)

    render json: @order
  end

  def update
    order_line = OrderUpdateService.add_or_update_order_line(@order, order_params)

    render json: order_line
  end

  private

  def find_or_create
    return @order = Order.create unless params[:id]

    @order = Order.find_by_id(params[:id])
    not_found unless @order
  end

  def order_params
    params.require(:line).permit(:item_id, :quantity)
  end

  def discount_params
    {
      current_user_time_zone: current_user_time_zone
    }
  end
end
