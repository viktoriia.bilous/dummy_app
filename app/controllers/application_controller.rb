class ApplicationController < ActionController::API
  before_action :current_user_time_zone

  def current_user_time_zone
    @current_user_time_zone = params[:time_zone] || 0
  end

  def not_found
    head 404
  end
end
