class OrderUpdateService
  attr_reader :order, :line

  def self.add_or_update_order_line(order, line)
    new(order, line).add_or_update_order_line
  end

  def initialize(order, line)
    @order = order
    @line = line
  end

  def add_or_update_order_line
    return order.order_lines.create(line) unless order_line

    order_line.update(quantity: line[:quantity])
    order_line
  end

  private

  def order_line
    @order_line ||= order.order_lines.find_by(item_id: line[:item_id])
  end
end
