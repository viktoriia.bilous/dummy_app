class OrderPriceService
  attr_reader :order

  def self.update_price_and_discount(order)
    new(order).update_price_and_discount
  end

  def initialize(order)
    @order = order
  end

  def update_price_and_discount
    order.update(total_price: total_price, total_discount: total_discount)
  end

  private

  def total_price
    order.order_lines.inject(0) do |total_price, line|
      total_price + line.price * line.quantity
    end
  end

  def total_discount
    order.order_lines.inject(0) do |total_discount, line|
      total_discount + line.price * line.quantity * line.discount / 100
    end
  end
end
