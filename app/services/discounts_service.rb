class DiscountsService
  attr_reader :order, :params

  DISCOUNTS = [
    EveningFoodDiscount,
    KeyboardMouseDiscount,
    WhiteItemsDiscount
  ].freeze

  def self.apply_discounts(order, params = {})
    new(order, params).apply_discounts
  end

  def initialize(order, params = {})
    @order = order
    @params = params
  end

  def apply_discounts
    order.order_lines.each { |line| line.update(discount: 0) }

    DISCOUNTS.each { |discount| discount.call(order, params) }
  end
end
