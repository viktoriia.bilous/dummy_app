class OrderLine < ApplicationRecord
  belongs_to :item
  belongs_to :order

  validates :item, :order, :quantity, presence: true

  before_create :set_price

  private

  def set_price
    self.price = item.price
  end
end
