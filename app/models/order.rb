class Order < ApplicationRecord
  has_many :order_lines, dependent: :destroy
  belongs_to :payment_info, optional: true
  belongs_to :shipping_info, optional: true

  enum status: [:draft, :placed, :paid, :shipped, :completed]
end
