class Category < ApplicationRecord
  has_many :items

  FOOD        = 1
  SHOES       = 2
  JACKET      = 3
  HOME_DECORE = 4
  SMARTPHONE  = 5
  LAPTOP      = 6
  KEYBOARD    = 7
  MOUSE       = 8

  def food?
    id == FOOD
  end

  def keyboard?
    id == KEYBOARD
  end

  def mouse?
    id == MOUSE
  end
end
