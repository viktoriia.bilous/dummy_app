class Item < ApplicationRecord
  belongs_to :category, counter_cache: true
  enum color: [:non, :white, :black, :red, :green, :blue, :yellow]

  validates :name, :price, :category, presence: true
end
