class CreateItems < ActiveRecord::Migration[5.2]
  def change
    create_table :items do |t|
      t.string :name
      t.text :description
      t.integer :color, default: 0
      t.decimal :price, precision: 10, scale: 2
      t.references :category, foreign_key: true

      t.timestamps
    end
  end
end
