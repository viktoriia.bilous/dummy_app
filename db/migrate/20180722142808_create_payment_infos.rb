class CreatePaymentInfos < ActiveRecord::Migration[5.2]
  def change
    create_table :payment_infos do |t|
      t.references :order, foreign_key: true

      t.timestamps
    end
  end
end
