class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.integer :status, default: 0
      t.decimal :total_price, precision: 10, scale: 2
      t.integer :total_discount

      t.timestamps
    end
  end
end
