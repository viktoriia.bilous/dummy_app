class CreateOrderLines < ActiveRecord::Migration[5.2]
  def change
    create_table :order_lines do |t|
      t.references :item, foreign_key: true
      t.references :order, foreign_key: true
      t.integer :quantity
      t.decimal :price, precision: 10, scale: 2
      t.integer :discount

      t.timestamps
    end
  end
end
