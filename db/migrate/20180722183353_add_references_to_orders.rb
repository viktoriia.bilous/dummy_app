class AddReferencesToOrders < ActiveRecord::Migration[5.2]
  def change
    add_reference :orders, :shipping_info, foreign_key: true
    add_reference :orders, :payment_info, foreign_key: true
  end
end
