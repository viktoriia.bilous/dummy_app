class ChangeOrders < ActiveRecord::Migration[5.2]
  def change
    change_column_null(:orders, :payment_info_id, true)
    change_column_null(:orders, :shipping_info_id, true)
  end
end
