class RemoveOrderIdFromShippingInfos < ActiveRecord::Migration[5.2]
  def change
    remove_reference :shipping_infos, :order, index: true
  end
end
