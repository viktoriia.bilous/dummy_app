class RemoveOrderIdFromPaymentInfos < ActiveRecord::Migration[5.2]
  def change
    remove_reference(:payment_infos, :order, index: true)
  end
end
