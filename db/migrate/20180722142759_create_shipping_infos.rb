class CreateShippingInfos < ActiveRecord::Migration[5.2]
  def change
    create_table :shipping_infos do |t|
      t.references :order, foreign_key: true

      t.timestamps
    end
  end
end
