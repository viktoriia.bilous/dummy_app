class ChangeOrderLineDefaultValues < ActiveRecord::Migration[5.2]
  def change
    change_column_default :order_lines, :quantity, from: nil, to: 1
    change_column_default :order_lines, :discount, from: nil, to: 0
  end
end
